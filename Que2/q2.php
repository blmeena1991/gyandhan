<?php
/**
 * Created by PhpStorm.
 * User: blmeena
 */
include_once("GreatCircle.php");

/**
 * Class MatchingFriend
 */
Class MatchingFriend extends GreatCircle{


    /**
     * @var int
     */
    private $_MAXTESTCASE=1000;
    /**
     * @var array
     */
    private $_nearbyFriends=array();
    /**
     * @var float
     */
    private $_fromLatitude=28.521134;
    /**
     * @var float
     */
    private $_fromLongitude=77.206567;
    /**
     * @var int
     */
    private $_fromDistance=100;//KM unit


    /**
     * @param $friendLatitude
     * @param $friendLongitude
     * @return bool
     */
    private function isNearbyFriend($friendLatitude, $friendLongitude){
        $nearDistance=GreatCircle::distance($this->_fromLatitude,$this->_fromLongitude,$friendLatitude,$friendLongitude);
        return $nearDistance <= $this->_fromDistance;
    }
    /**
     * @param $fileName
     */
    public function readFriendFile($fileName){
       $handle = fopen($fileName, "r");
       if ($handle) {
           while (($line = fgets($handle)) !== false) {
               $line = str_replace("\n", "", $line);
               $line=json_decode($line);
               if ($line === FALSE) {
                   continue;
               }
               if($this->validateFriend((array)$line)){
                  try{
                      if($this->isNearbyFriend($line->latitude,$line->longitude)){
                          $this->_nearbyFriends[]=(array)$line;
                      }
                  }catch(Exception $e){
                      echo $e->getMessage().PHP_EOL;
                      continue;
                  }
               }
           }
           fclose($handle);
       }
    }

    /**
     * @param $arr
     * @param $col
     * @param int $dir
     * return the sorted the array by id
     */
    public function sortArray(&$arr, $col, $dir = SORT_ASC) {
        $sort_col = array();
        foreach ($arr as $key=> $row) {
            $sort_col[$key] = $row[$col];
        }
        array_multisort($sort_col, $dir, $arr);
    }

    /**
     * @return array
     */
    public function getFriendList(){
       return $this->_nearbyFriends;
    }

    /* Checks whether friend object is valid*/
    /**
     * @param array $friend
     * @return bool
     * Validate the friend info
     */
    private function validateFriend(array $friend){
        $friend_attributes=array('id', 'name','latitude', 'longitude');
        foreach($friend_attributes as $attribute){
           if(!in_array($attribute,array_keys($friend))) {
               return false;
           }
        }
        return true;
    }

    /**
     * @param $fileName
     * Generate the test case
     */
    public function generateTestCase($fileName){
        $fileHandle = file_exists($fileName) ? fopen($fileName, 'a') : fopen($fileName, 'w');
        if ($fileHandle !== false) {
            ftruncate($fileHandle, 0);
            fclose($fileHandle);
        }

        for($i=1;$i<=$this->_MAXTESTCASE;$i++){
          $friend=array('id'=>GreatCircle::generateUniqueId(),'name'=>GreatCircle::generateRandomString(rand(6,10)),'latitude'=>GreatCircle::generateRandomNumber(-90,90),'longitude'=>GreatCircle::generateRandomNumber(-180,180));
          file_put_contents($fileName, json_encode($friend).PHP_EOL, FILE_APPEND | LOCK_EX);
      }

    }
}





//RUN the function

$matchingFriendObj=new MatchingFriend();


$matchingFriendObj->generateTestCase('friends.json');

$matchingFriendObj->readFriendFile('friends.json');

$friendList=$matchingFriendObj->getFriendList();

$matchingFriendObj->sortArray($friendList,'id');

if(!empty($friendList)){
    foreach($friendList as $friend){
        echo $friend['name'].':'.$friend['id'].PHP_EOL;
    }
}else{
    echo 'No near friend found';

}


