<?php
/**
 * Created by PhpStorm.
 * User: blmeena
 */

/**
 * Class TripletNumber
 */
Class TripletNumber{

    /**
     * @var bool
     */
    private $_foundNumbers=false;
    /**
     * TripletNumber constructor.
     */
    public function __construct()
    {
       return;
    }

    /**
     * @param array $array
     * @return array
     */
    private function quickSort(array $array) {
        if (count($array) == 0) {
            return $array;
        }
        $pivot = $array[0];
        $left = $right = array();
        for($i = 1; $i < count($array); $i ++) {
            if ($array[$i] < $pivot) {
                $left[] = $array[$i];
            } else {
                $right[] = $array[$i];
            }
        }
        return array_merge($this->quickSort($left), array(
            $pivot
        ), $this->quickSort($right));
    }


    /**
     * @param array $array
     * @param $sum
     * @return bool
     */
    public function findTripletNumber(array $array, $sum){
        $array_size=count($array);
        /* Sort the elements */
        $sortedArray=$this->quickSort($array);

        /* Now fix the first element one by one and find the
           other two elements */
        for ($i=0; $i<$array_size-2; $i++) {

            // To find the other two elements, start two index variables from two corners of the array and move them toward each other
            $l = $i + 1;  // index of the first element in the remaining elements
            $r = $array_size-1; // index of the last element
            while ($l < $r) {
                if( $sortedArray[$i] + $sortedArray[$l] + $sortedArray[$r] == $sum) {
                    echo $sortedArray[$i].','.$sortedArray[$l].',',$sortedArray[$r].PHP_EOL;
                    $this->_foundNumbers= true;
                    $l++;
                    $r--;
                }
                else if ($sortedArray[$i] + $sortedArray[$l] + $sortedArray[$r] < $sum)
                    $l++;
                else
                    $r--;
            }
        }
        // If we reach here, then no triplet was found
        if ($this->_foundNumbers== false)
            echo  " No Triplet Found";
    }

}

echo  'Test Case 1:'.PHP_EOL;

$input= array(0, -1, 2, -3, 1);
$sum=0;

echo  'Input Array Sample : '.implode(',',$input).PHP_EOL;
echo  'Sum Value : '.$sum.PHP_EOL;
echo  'Output:'.PHP_EOL;
$tripletNumberObj=new TripletNumber();
$tripletNumberObj->findTripletNumber($input,$sum);



echo  "\n".'Test Case 2:'.PHP_EOL;

$input= array(2,4,5,11,6,45,23,12,24,13,1);
$sum=16;

echo  'Input Array Sample : '.implode(',',$input).PHP_EOL;
echo  'Sum Value : '.$sum.PHP_EOL;
echo  'Output:'.PHP_EOL;
$tripletNumberObj=new TripletNumber();
$tripletNumberObj->findTripletNumber($input,$sum);

